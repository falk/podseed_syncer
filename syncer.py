#! /usr/bin/env python

import sys
import time
import logging
import zmq
import yaml
import boto3

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# bind zmq context
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:5656")

# create s3 config
with open('config.yaml') as fi:
    config = yaml.safe_load(fi)

s3 = boto3.client('s3', 
endpoint_url = config['s3']['endpoint'],
aws_access_key_id = config['s3']['access_key_id'],
aws_secret_access_key = config['s3']['secret_access_key'])

class _CustomHandler(FileSystemEventHandler): 
    def on_created(self, event):
    # cleanup filename and path
        filename = event.src_path
        path = filename.split("/")
        path.remove("opt")
        path.remove("podcast_cdn")
        message = "/".join(path)
    # upload file to S3
        with open(filename, "rb") as f:
            s3.upload_fileobj(f, config['s3']['default_bucket'], message)
    # notify other nodes by zmq
        print("Sending message %s" % message)
        socket.send_string(message)

if __name__ == "__main__":
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = _CustomHandler()

    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()