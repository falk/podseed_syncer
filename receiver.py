#! /usr/bin/env python

import sys
import time
import logging
import zmq

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://localhost:5656")
socket.subscribe(b'')

baseurl = 'https://podseed.s3.eu-central.wasabisys.com/'

path = sys.argv[1] if len(sys.argv) > 1 else '.'

while True:
    message = socket.recv_string()
    print("Got message %s" % message)
